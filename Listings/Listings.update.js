function update(item, user, request) {
    //shoud just be set completed flag to false request that calls this
    tables.getTable('Listings').where({
        userId: item.userId,
        complete: false
    }).read({
        success: insertItemIfNotComplete
    });
    function insertItemIfNotComplete(existingItems) {
        if (existingItems.length < 1) {
            request.respond(statusCodes.CONFLICT,
                "You don't have a pizza listing to get rid of");
        } else {
            request.execute();
        }
    }
}
