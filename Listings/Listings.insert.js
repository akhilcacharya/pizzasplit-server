function insert(item, user, request) {
    item.userId = user.userId;
    item.complete = false;
    tables.getTable('Listings').where({
                userId: item.userId,
                complete: false
            }).read({
                success: function(results)
                  {
                      insertItemIfNotComplete(results);
                  }
            });

        function insertItemIfNotComplete(existingItems) {
            if (existingItems.length > 0) {
                request.respond(statusCodes.CONFLICT,
                    "Multiple listings are not allowed.");
            } else {
                request.execute();
            }
        }
}
