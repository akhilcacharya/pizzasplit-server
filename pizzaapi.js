exports.post = function(request, response) {
    var passedLat = request.body.lat;
    var passedLon = request.body.lon; //need to use %d to pass in these to the sql obj
    console.log("TEST %o", request.body);
    var mssql = request.service.mssql;
    var sql = "SELECT brand,type,lat,lon,time,userId FROM listings WHERE __deleted = 0 AND lat BETWEEN (? - 0.1) AND (? + 0.1) AND lon BETWEEN (? - 0.1) AND (? + 0.1)";

    mssql.query(sql, [passedLat,passedLat,passedLon,passedLon],{
        success: function(results) {
            response.send(200,  {list: results});
    }
    });
};
